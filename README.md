# My Frontend Roadmap From Middle+ to Senior Angular Software Developer

#### CI/CD
- [ ] [CI|CD](https://gitlab.com/Goodzonchik/frontend-roadmap/-/blob/main/CI-CD/CI-CD%20concepts.md)
- [ ] [Gitlab CI](https://gitlab.com/Goodzonchik/frontend-roadmap/-/blob/main/CI-CD/gitlab-ci.md)

### HTML
- [ ] async|defer attribute and other

### CSS|Styling
- [ ] CSS variables
- [ ] Viewport
- [ ] work with photoshom|figma

#### JavaScript/TypeScript
- [ ] Typing types dynamic|static and other
- [ ] JavaScript
  + https://github.com/getify/You-Dont-Know-JS
  - [ ] Proxy
  - [ ] Generator
  - [ ] EventLoop
    -  [ ] RequestAnimationFrame
  - [ ] Promises/Fetch/Async-await
  - [ ] Property Descriptor
  - [ ] Typed Arrays
  - [ ] Intl
- [ ] TypeScript
  + https://ghaiklor.github.io/type-challenges-solutions/en/
  - [ ] public|privae|static and other modificator
  - [ ] Interface vs. Type alias
  - [ ] Const assertion
  - [ ] Type Union and type intersections
  - [ ] Utility types
  - [ ] Conditional types
  - [ ] Mapped types
  - [ ] Recursion types
  - [ ] Recursive types
  - [ ] Type Guards
  - [ ] Description of the domain area
  - [ ] Contract development


#### Testing

- [ ] Manual and Automation Theory
  - [ ] Testing model
- [ ] Jest
- [ ] Cypress/Playwing
- [ ]  Mocking/Stubing
- [ ] CodeCoverage + tooling
- [ ] Best practice unit-test/integration tests.

#### Server Side

- [ ] [Nginx](https://nginx.org/)
- [ ] Kubernetes/k8s
  - [ ] Get logs and base info about pods/deployment and other
  - [ ] kubectl
  - [ ] kubernetes in docker
- [ ] S3
- [ ] CDN

#### Developer Expirience

- [Storybook](https://storybook.js.org/)
- [WebPack](https://gitlab.com/Goodzonchik/frontend-roadmap/-/blob/main/Developer%20Expirience/Webpack.md)
- Nx
- Lerna
- Microfrontends / Module Federation
- SCV|Git
  - [ ] [Semver](https://semver.org/lang/ru/)
  - [ ] [Commit convention](https://www.conventionalcommits.org/en/v1.0.0/)
- Server Side Rendering
- Gitlab
- Code metrics
- App Logging (Graphana, Elastic etc)
- [Docker](https://gitlab.com/Goodzonchik/frontend-roadmap/-/blob/main/Developer%20Expirience/Docker.md)
- Prettier
- husky
- EsLint|TsLint
- NPM
  - package.json|package-lock
  - base command
  - Npm/yarn/npx/pnpm
  - Generate and publish npm-package  (skill  )
- JS Doc

#### Frameworks and libraries

- Angular
  - [ ] Read about Angular.json
  - [ ] Signals
  - [ ] Zone.js
    + https://angular.io/guide/zone
    - [ ] runOutZone
  -  [ ] Detect Changes
    - [ ] Change Detection Ref Deep
  - [ ] Refs: TemplateRef, ViewContainerRef, ForwardRef and etc.
  - [ ] Structural directives sintax
  - [ ] Service and component lifecycle in deeps
  - [ ] Providers in component, module, root and provideIn
  - [ ] Lazy loading injector
  - [ ] inject function
  - Tools
    - Schematics
      - [ ] Connect Schematics collections to my app
      - [ ] Write Schematics
    - [ ] Write angular builder
    - [ ] Read about AST
    - [ ] Write simple migration
  - Angular DI
    - [ ] Read about forwardRef
    - [ ] Write custom form control
    - [ ] Read about and use inject function
    - [ ] Read about multi providers
  - [ ] Angular CDK (Virtual Scroll, etc)
- RxJs
  - [ ] Base
    - [ ] Hot/Cold observables
  - [ ] Schedulers
  - [ ] Marble
    - [ ] Marble diagram
    - [ ] Marble testing
  - Rare used operator such as scan|reduce|distinct
- NgRx
  - [ ] Read about sate-management
  - [ ] Read about concepts
- Date libs: moment|luxon|date-fns and Temporal API
- [ ] CoreJs
- [ ] d3.js

#### Graphics|Browser render

- [ ] Canvas
- [ ] WebGL/OpenGL
- [ ] Reflow|Repaint|Layout
- [ ] QueueMicrotask|RequestAnimationFrame|RequestIdleCallback|Compositor thread 

#### Messaging between Client and Server

- [ ] Server Send Events
- [ ] Web Socket
  - [Web Socket](https://javascript.info/websocket)
  
  Questions:
  - What is Web Socket?
  - How Web Socket work?

#### PWA
- [ ] WebWorkers
- [ ] Service Workers


#### Network
- [ ] cUrl
- [ ] WebRtc
- [ ] Wireshark
- [ ] Model OSI
- [ ] Protocols HTTP|HTTPS and other
- [ ] Protocols TCP|IP and other
- [ ] SSL|TSL
- [ ] DNS

### Soft Skills

- Interview
  - [ ] Interview
- [ ] Leading (team/tech)
- [ ] Presentation at the conference
- [ ] Write wiki
    - [ ] RFC
    - [ ] ADR
    - [ ] Postmortrem
    - [ ] Onboarding
    - [ ] Meeting note
- [ ] Onboarding
- [ ] Сonducting team rituals (dayly/weakly/retro and other)
- [ ] Manage team process
- [ ] Mentoring/Teaching
- [ ] Kanban/Agile etc

### Knowledge

- [ ] data structures
  + https://github.com/trekhleb/javascript-algorithms
- [ ] algoruthm
- [ ] System design
  - [ ] [C4 model](https://c4model.com/)
  - [ ] [12 factor app](https://12factor.net/)
  - [ ] Collection of requirements
- [ ] Backend
  - [ ] Node Js
  - [ ] Other language
- [ ] Data base
  - [ ] SQL
  - [ ] No SQL

- [ ] SRE
  - [What Is Site Reliability Engineering (SRE)?](https://aws.amazon.com/what-is/sre/?nc1=h_ls)
    
    Questions: 
    What Is Site Reliability Engineering (SRE)?
    What is observability in site reliability engineering?
    What is monitoring in site reliability engineering?
    What is SLA?
    What is SLO?
    What is SLI?

- [ ] Standarts (ISO 9000, CMMI)
- [ ] Math
- [ ] AI
- Task flows
  - [ ] Jira
- [ ] Linux Comand
- [ ] bash
- [ ] BFF
- [ ] API Gateway
- [ ] Principal
  - [Programming principles](https://github.com/webpro/programming-principles)

### Security

### Interesting
- [ ] Up container with LibreOffice/Collabora for open .doc and .exls files in browser
- [ ] WebAssembly