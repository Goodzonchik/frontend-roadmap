### Webpack

- [ ] What is webpack and read concepts
    - [Concepts](https://webpack.js.org/concepts/)
    
    Questions:
    - What is Webpack?
    - Concepts?

- [ ] Entry Points

    - [Concepts](https://webpack.js.org/concepts/)
    - [Entry Points](https://webpack.js.org/concepts/entry-points/)

    Questions:
    - What is entry points?
    - How write:
        - Single Entry
        - Object Syntax
        - EntryDescription object
    - Using scenarios?

- [ ] Output

    - [Concepts](https://webpack.js.org/concepts/)
    - [Output](https://webpack.js.org/concepts/output/)

    Questions:
    - What is Output?
    - How write?
    - How write Multiple Entry Points?