### Docker
- [ ] Read about hypervisor (5 min)
    - ["What is hypervisor?"](https://aws.amazon.com/what-is/hypervisor/), ([RU](https://aws.amazon.com/ru/what-is/hypervisor/))
    
    Questions:
    - What is hypervisor?
    - How a hypervisor works?
- [ ] Read about WSL/WSL2 (5 min)
    - [What is the Windows Subsystem for Linux?](https://learn.microsoft.com/en-us/windows/wsl/about), ([RU](https://learn.microsoft.com/ru-ru/windows/wsl/about))

    Questions:
    - What is WSL?
    - How a WSL works?

- [ ] Read about concepts containers and virtual machines (5 min)
    - ["Containers vs. virtual machines"](https://learn.microsoft.com/en-us/virtualization/windowscontainers/about/containers-vs-vm), ([RU](https://learn.microsoft.com/ru-ru/virtualization/windowscontainers/about/containers-vs-vm))
    - ["Containers vs. virtual machines"](https://www.atlassian.com/microservices/cloud-computing/containers-vs-vms), ([RU](https://www.atlassian.com/ru/microservices/cloud-computing/containers-vs-vms))

    Questions:
    - What is container?
    - What is virtual machines?
    - What diffirence between?

- [ ] Read about Docker (10 min)
    - ["Docker overview"](https://docs.docker.com/get-started/overview/)
    - ["What is Docker?"](https://learn.microsoft.com/en-us/dotnet/architecture/microservices/container-docker-introduction/docker-defined), ([RU](https://learn.microsoft.com/ru-ru/dotnet/architecture/microservices/container-docker-introduction/docker-defined))

    Questions:
    - What is docker?
    - What is image, contaainer?
    - How its works?

- [ ] Containerize first application (25 min)
    - ["Containerize an application"](https://docs.docker.com/get-started/02_our_app/)
    - ["Update the application"](https://docs.docker.com/get-started/03_updating_app/)

    Questions:
    - How build/rebuild docker-image?
    - How start|stop|delete container?
    - Command line for work with contanier|docker-image?

    Skills:
    - Build/rebuild docker-image
    - Start|stop|delete container

- [ ] Publish (push) you docker-image (10 min)
    - [""](https://docs.docker.com/get-started/04_sharing_app/

    Questions:
    - What is Docker-Hub?
    - What is Docker-ID?
    - How push you image in Docker hub?
    - How use image from Docker hub?

    Skills:
    - Push you image in Docker hub
    - Pull you image in Docker hub