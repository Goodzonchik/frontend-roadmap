# Developer Expirience

- Storybook
  - [ ] Read about feature
  - [ ] Add storybook for apps
- [ ] WebPack and etc (draft)
- Nx
  - [ ] Create monorepos
  - [ ] Generator
- [ ] Microfrontends / Module Federation
- [ ] Git
- Server Side Rendering
  - [ ] Read theory
    + https://angular.io/guide/universal
  - [ ] Angular Universal
- Gitlab
  - [ ] Manage repository
    1. Protected branch
    2. Approvers
- [ ] Code metrics
- [ ] App Logging (Graphana, Elastic etc)
- [Docker](https://gitlab.com/Goodzonchik/frontend-roadmap/-/blob/main/Developer%20Expirience/Docker.md)
  - [x] Read about Hypervisor
  - [x] Read about WSL
  - [x] Read about concepts containers and virtual machines
  - [x] Read about Docker
  - [ ] Create docker image
  - [ ] Learn config
  - [ ] Docker CLI
  - [ ] Docker Hub
  - [ ] Docker Compose
  - [ ] Docker in Docker
  - [ ] Kaniko
  - [ ] Docker on Kubernetes [Enable kubernetes on docker desctop](https://docs.docker.com/desktop/kubernetes/#enable-kubernetes)