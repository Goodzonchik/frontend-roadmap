### Gitlab CI

- [ ] Read about Gitlab CI
    - [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

    Questions:    
    - What is Gitlab CI?

- [ ] Read about pipelines
    - [Pipelines](https://docs.gitlab.com/ee/ci/pipelines/index.html)
    
    Questions:
    - What is Pipeline?
    - What pipeline comprise?
    - Types of pipelines?
 
    Skills:
    - Run pipelines
    - Search piplines
    - Run pipelines with Variables
    - View jobs dependencies

- [ ] Read about jobs
    [Jobs] https://docs.gitlab.com/ee/ci/jobs/
 
    Questions:
    - What is jobs?
    - How see jobs?
    - How group jobs?