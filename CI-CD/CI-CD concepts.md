### CI/CD concepts

- [ ] Read about CI/CD concepts
    - [CI/CD concepts](https://docs.gitlab.com/ee/ci/introduction/index.html#continuous-integration)
    
    Questions:
    - What is CI?
    - What is Continuous Dilivery?
    - What is Continuous Deployment?
    - Base CI/CD workflow?

    - [Continuous integration](https://en.wikipedia.org/wiki/Continuous_integration)
       
    Questions:
    - What is CI?
    - Principles CI?
    - Costs and benefits CI?

- [ ] Deployment strategies

    - [Deployment Strategies](https://www.baeldung.com/ops/deployment-strategies)
    - [Deployment Strategies: 6 Explained in Depth](https://www.plutora.com/blog/deployment-strategies-6-explained-in-depth)

    Questions:
    - How work Blue/green deployment?
    - How work Canary deployment?
    - How work Rolling Update?
    - How work Shadow deployment?
    - How work A|B-testing deployment?
    - How work Recreate deployment?
    - Pros and cons deployments?